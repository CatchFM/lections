# CTF resourses
 
## To read
[Конференция BLACK HAT USA. Как Голливудский хакер использует камеры наблюдения](https://m.habr.com/ru/company/ua-hosting/blog/427367/)
 
[Курсы команды Hackerdom · Курс молодого CTF бойца v 1.5](http://kmb.ufoctf.ru/hackerdom/main.html)
 
[Разбор вступительных испытаний для стажеров «Digital Security»](https://habr.com/ru/company/dsec/blog/461077/)
 
[Конференция BLACK HAT USA. Как Голливудский хакер использует камеры наблюдения](https://habr.com/ru/company/ua-hosting/blog/427367/%3E/)

[Secjuice](https://www.secjuice.com/)
 
## To watch
 
[SpbCTF channel](https://www.youtube.com/channel/UCaqi7baKiDd8jMR_fyjjccw/videos)
 
[NeoQuest lections](https://www.youtube.com/channel/UCgQUxiK2oYU4kQOy9oJnMpQ/videos)
## To follow
[Cyber**it | tg](https://t.me/cybershit)
 
[Xaker](https://t.me/hacker_frei)
 
[CTF News | ВКонтакте](https://vk.com/ctfnews)
 
 
 
## To solve
 
[picoCTF](https://picoctf.com/)
 
[Shellter Labs](https://shellterlabs.com)
 
[Crackmes](https://crackmes.one/)
 
[forkbomb.ru](https://forkbomb.ru)
 
[Hackerdom Trainings ](https://training.hackerdom.ru)

[Home \| Hacker101](https://www.hacker101.com/)
 
[OverTheWire: Wargames](http://overthewire.org/wargames/)
 
[Root-me](https://www.root-me.org/?lang=en)
 
[Hack The Box](https://www.hackthebox.eu/)
 
[Embedded Security CTF](https://microcorruption.com/login)
 
[squallygame](https://squallygame.com/en)
 
[Hacksplaining](https://www.hacksplaining.com)
 
[TryHackMe](https://tryhackme.com/)
 
-----
 
 
# Categories
Первый и главный совет: начните использовать Unix систему
Лучшим первым шагом будет установка *Kali linux* на виртуальную машину.
Ловите отличный [гайд](https://www.youtube.com/watch?v=nZ3mDnpZeic) на эту тему.
 
## Web
 
#### keywords
Cookies, XSS, SQL injection, File inclusion, IDOR, HTTP headers, GET/POST, JSON, PHP, DNS, RCE,
#### To read
[Web tools, или с чего начать пентестеру? / Блог компании Digital Security / Хабр](https://habr.com/ru/company/dsec/blog/452836/)

[Owasp](https://www.owasp.org/index.php/Main_Page)
 
#### To use
[Burp Suite Scanner](https://portswigger.net/burp)
[dirsearch](https://github.com/maurosoria/dirsearch)
[curl](https://curl.haxx.se/)
 
#### To solve
[OWASP Juice Shop](https://github.com/bkimminich/juice-shop)
 
[Burpsuit](https://portswigger.net/)
 
[XSS game](https://xss-game.appspot.com/)
 
[Natas challenge](http://overthewire.org/wargames/natas/)

-----
 
## Forensic
#### keywords
Hex view, magic bytes, exif, Hex editor
#### To read
[Методы обнаружения «склеенных» файлов / Хабр](https://m.habr.com/ru/company/infowatch/blog/337084/)
 
[Практические приёмы работы в Wireshark / Хабр](https://m.habr.com/ru/company/ruvds/blog/416537/)
 
[Как восстановить утерянный пароль к архиву с помощью видеокарты / Хабр](https://m.habr.com/ru/company/infowatch/blog/352396/)
 
[Работаем в терминале Linux как профи: подборка полезных команд](https://tproger.ru/articles/useful-linux-commands/amp/)
 
[Подборка эзотерических языков программирования \| ВКонтакте](https://vk.com/@tproger-esoteric-programming)

[71 команда Linux на все случаи жизни. Ну почти - Лайфхакер](https://lifehacker.ru/komandy-linux/)
 
#### To use
[binwalk: Firmware Analysis Tool](https://github.com/ReFirmLabs/binwalk)

[010 Editors](https://www.sweetscape.com/010editor/)

[The Sleuth Kit](https://www.sleuthkit.org/)
 
-----
 
 
## Reverse
#### keywords
assembler, obfuscation, packer, elf linux, pe windows, debug, reverse engeneering
 
#### To read
[Изучаем С используя GDB / Хабр](https://m.habr.com/ru/post/181738/)
[Разбираемся с IDA free](http://yasha.su/VVRSNIIP_12.pdf)
#### To use
r2, IDA Pro, WinDBG, x64dbg, procmon, hiew, gdb, dotPeek, fernflower\jd-gui
 
#### To solve
[pwnable](http://pwnable.kr)

-----

## PWN
#### keywords
pwn, binary vulnerabilities, buffer overflow, format string, use after free, ROP gadget
 
#### To use
one_gadget, pwntools, r2, IDA Pro, WinDBG, x64dbg, gdb
 
#### To solve

[pwnable](http://pwnable.kr)

## PPC
 
#### To use
[pwntools](http://docs.pwntools.com/en/stable/)
 
-----
 
 
## Crypto
#### keywords
RSA, hashes, Caesar cipher, asymmetric/symmetric cryptography, public/private key
 
#### To read
[Криптографические атаки: объяснение для смятённых умов / Хабр](https://m.habr.com/ru/post/462437/)
 
[Разделяй и властвуй, или зачем нужны схемы разделения секрета](https://www.youtube.com/watch?v=d9rYPTLCkq8)

#### To use
[Прекраснейший инструмент с огромным выбором различных дешифраторов](https://gchq.github.io/CyberChef/)
 
#### To solve
[The Cryptopals Crypto Challenges](https://cryptopals.com)
 
-----
 
## Stegano
#### keywords
least significant bit,
 
 
#### To use
[StegSolve](http://www.caesum.com/handbook/Stegsolve.jar)
[StegHide](http://steghide.sourceforge.net/)
 
-----
 
 
## OSINT
#### keywords
Google, Яндес.Люди

#### To read
[Как найти аккаунты и личные данные человека в сети :: Инструменты для деанона по открытым источникам :: Блог Вастрик.ру](https://vas3k.ru/blog/389/)
 
[По следам кибердетектива / Хабр](https://habr.com/ru/post/338078/)
 
[Разбор конкурса «Конкурентная разведка» на PHDays 9 / Блог компании Positive Technologies / Хабр](https://habr.com/ru/company/pt/blog/456640/)
 
[Подборка кейсов поиска информации в интернете](https://github.com/netstalking-core/netstalking-osint/blob/master/README.md#-%D0%9D%D0%B0-%D0%B0%D0%BD%D0%B3%D0%BB%D0%B8%D0%B9%D1%81%D0%BA%D0%BE%D0%BC)
 

#### To use
[namecheck](https://namechk.com/)
[Shodan](https://www.shodan.io/)
[Большой набор инфструментов, источников и баз данных](https://github.com/jivoi/awesome-osint)

#### To solve
[TryHackMe комната с заданиями OSint](https://tryhackme.com/room/ohsint)

-----
 
 
## Network
#### keywords
OSI и TCP/IP, DNS, DHCP
 
#### To read
Курсы Cisco CCNA, Huawei HCNA


 
#### To use
[WireShark](https://www.wireshark.org/)
 
#### To solve
[Комната на TryHackMe с заданиями по wireshark](https://tryhackme.com/room/wirectf)

[Проверка знаний по сетям, маскам подсети](https://tryhackme.com/room/bpnetworking)